/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package vegetable;

/**
 *
 * @author mitpa
 */
public enum VegetableType
{
    BROCCOLI,
    SPINACH,
    TOMATO,
    CARROT,
    BEET
}