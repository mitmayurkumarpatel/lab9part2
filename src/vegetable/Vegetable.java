package vegetable;

/**
 *
 * @author mitpa
 */

    public abstract class Vegetable {
    
    //attribute(s)
    protected String color;
    protected double size;
    
    
    
    //constructor(s)
        public Vegetable(String color, double size) {
        this.color = color;
        this.size = size;
    } 
    
    //getter(s)
    public abstract String getColor();
    public abstract double getSize();
    
    //method(s)
    public abstract boolean isRipe();

}


