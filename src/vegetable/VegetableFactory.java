package vegetable;
/**
 *
 * @author mitpa
 */
public class VegetableFactory 
{
    private static VegetableFactory factory;
    
    //private default constructor --> so that  no one can instantiate from outside
    private VegetableFactory()
    {}
    
    public static VegetableFactory getInstance()
    {
        if(factory==null)
            factory = new VegetableFactory();
        return factory;
    }
   
    public Vegetable getVegetable(VegetableType type,String color,double size)
    {
        switch( type )
        {
            case CARROT : return new Carrot(color, size);
        }
        return null;
    }
}
