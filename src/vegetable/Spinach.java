package vegetable;

/**
 *
 * @author mitpa
 */

    public class Spinach extends Vegetable {
    
    //constructor(s)
    public Spinach(String color, double size) {
        super(color, size);
    }
    
    //getters
    @Override    
    public double getSize() {return super.size;}
    public String getColor() {return super.color;}    
    
    //method(s)
    @Override
    public boolean isRipe() {
        if(super.size == 3.5 && super.color == "light green"){
            return true;
        } else {
            return false;
        }
    }    
}


