package vegetable;

/**
 *
 * @author mitpa
 */

    public class Tomato extends Vegetable {
    
    //constructor(s)
    public Tomato(String color, double size) {
        super(color, size);
    }
    
    //getters
    @Override    
    public double getSize() {return super.size;}
    public String getColor() {return super.color;}    
    
    //method(s)
    @Override
    public boolean isRipe() {
        if(super.size == 4.5 && super.color == "red"){
            return true;
        } else {
            return false;
        }
    }    
}


