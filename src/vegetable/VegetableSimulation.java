package vegetable;

/**
 *
 * @author mitpa
 */
public class VegetableSimulation
{
    public static void main(String[] args)
    {
        VegetableFactory factory = VegetableFactory.getInstance();
        
        Vegetable carrot = factory.getVegetable(VegetableType.CARROT, "YELLOW", 2);
        
        System.out.println("Colour of" +carrot.getClass()+" is " +carrot.getColor() +" and is it ripe ?" +carrot.isRipe());
    }
    
}
